import random
import re
import socket
from typing import List

from typing.io import TextIO

HOST = "127.0.0.1"
PORT = 1234
symbols = ("a", "b", "c", "d", "e", "f", "g", "h")


class CoordinateManager:
    def __init__(self, figure: str, coord: str):
        if figure.lower() not in ("p", "q", "k", "n", "b", "r"):
            raise ValueError("Figure is incorrect")
        if ord(coord[1]) not in range(49, 57) or len(coord) > 2:
            raise ValueError("Coordinates are incorrect")
        self.figure = figure
        self.coord = coord
        self.symbols = ("a", "b", "c", "d", "e", "f", "g", "h")

    @staticmethod
    def increase_num(coord: str) -> str:
        return f"{coord[0]}{int(coord[-1]) + 1 if int(coord[-1]) < 8 else int(coord[-1])}"

    @staticmethod
    def decrease_num(coord: str) -> str:
        return f"{coord[0]}{int(coord[-1]) - 1 if int(coord[-1]) > 1 else int(coord[-1])}"

    @staticmethod
    def increase_word(coord: str) -> str:
        return f"{chr(ord(coord[0]) + 1) if ord(coord[0]) < 104 else coord[0]}{int(coord[-1])}"

    @staticmethod
    def decrease_word(coord: str) -> str:
        return f"{chr(ord(coord[0]) - 1) if ord(coord[0]) > 97 else coord[0]}{int(coord[-1])}"

    def oblique_right_stroke(self, coord: str) -> str:
        return self.increase_word(self.increase_num(coord))

    def oblique_left_stroke(self, coord: str) -> str:
        return self.decrease_word(self.increase_num(coord))

    def oblique_left_stroke_bottom(self, coord: str) -> str:
        return self.decrease_word(self.decrease_num(coord))

    def oblique_right_stroke_bottom(self, coord: str) -> str:
        return self.increase_word(self.decrease_num(coord))

    def get_all_moves_obliquely(self) -> List[str]:
        result = []
        new_move = self.coord
        while "8" not in new_move and "h" not in new_move:
            new_move = self.oblique_right_stroke(new_move)
            result.append(new_move)
        new_move = self.coord
        while "8" not in new_move and "a" not in new_move:
            new_move = self.oblique_left_stroke(new_move)
            result.append(new_move)
        new_move = self.coord
        while "1" not in new_move and "a" not in new_move:
            new_move = self.oblique_left_stroke_bottom(new_move)
            result.append(new_move)
        new_move = self.coord
        while "1" not in new_move and "h" not in new_move:
            new_move = self.oblique_right_stroke_bottom(new_move)
            result.append(new_move)
        return result

    def get_all_moves_straight_line(self):
        return [f"{self.coord[0]}{i}" for i in range(1, 9)] + [f"{s}{self.coord[1]}" for s in symbols]

    def get_correct_moves(self) -> List[str]:
        if self.figure == "P":
            result = self.increase_num(self.coord)
            return [result, self.increase_word(result), self.decrease_word(result)]
        elif self.figure == "p":
            result = self.decrease_num(self.coord)
            return [result, self.increase_word(result), self.decrease_word(result)]
        elif self.figure.lower() == "r":
            return self.get_all_moves_straight_line()
        elif self.figure.lower() == "b":
            return self.get_all_moves_obliquely()
        elif self.figure.lower() == "n":
            return [
                self.increase_num(self.oblique_left_stroke(self.coord)),
                self.increase_num(self.oblique_right_stroke(self.coord)),
                self.increase_word(self.oblique_right_stroke(self.coord)),
                self.decrease_word(self.oblique_left_stroke(self.coord)),
                self.decrease_num(self.oblique_left_stroke_bottom(self.coord)),
                self.decrease_num(self.oblique_right_stroke_bottom(self.coord)),
                self.increase_word(self.oblique_right_stroke_bottom(self.coord)),
                self.decrease_word(self.oblique_left_stroke_bottom(self.coord))
            ]
        elif self.figure.lower() == "k":
            return [
                self.oblique_left_stroke(self.coord), self.oblique_left_stroke_bottom(self.coord),
                self.oblique_right_stroke(self.coord),
                self.oblique_right_stroke_bottom(self.coord), self.increase_num(self.coord),
                self.increase_word(self.coord),
                self.decrease_num(self.coord),
                self.decrease_word(self.coord)
            ]
        elif self.figure.lower() == "q":
            return self.get_all_moves_straight_line() + self.get_all_moves_obliquely()


def send_request_and_get_responce(write_file: TextIO, read_file: TextIO, command: str) -> str:
    write_file.write(f"{command}\n")
    write_file.flush()
    return read_file.readline().strip()


def get_possible_coordinates(moves: List[str], write_file, read_file, my_color: str) -> (List[str], List[str]):
    while True:
        move_from = random.choice(moves)
        selected_figure = send_request_and_get_responce(write_file, read_file, f"get {move_from}")
        if (my_color == "white" and selected_figure in (
                "P", "Q", "K", "N", "B", "R")) or (my_color == "black" and selected_figure in (
                "p", "q", "k", "n", "b", "r")):
            return move_from, CoordinateManager(selected_figure, move_from).get_correct_moves()


def is_game_over(data: str) -> bool:
    return any(
        [re.search(r"victory", data), re.search(r"game_ended_you_lost", data), re.search(r"game_ended_you_won",
                                                                                         data)])


def is_success_connect(data: str) -> bool:
    return any([re.search(r"successfully_connected_to_room", data), re.search(r"new_room_created", data)])


def main(room_name: str):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        read_file = s.makefile(mode="r", encoding="utf-8")
        write_file = s.makefile(mode="w", encoding="utf-8")

        moves = [f"{s}{i}" for s in symbols for i in range(1, 9)]

        while True:
            if is_success_connect(
                    send_request_and_get_responce(write_file, read_file, f"connect_to_room {room_name}")):
                break

        while True:
            my_color = re.search(r"success (white|black)",
                                 send_request_and_get_responce(write_file, read_file, "start_game"))
            if my_color:
                my_color = my_color.group(1)
                break

        while True:
            move_from, move_to_coords = get_possible_coordinates(moves, write_file, read_file, my_color)
            if is_game_over(send_request_and_get_responce(write_file, read_file,
                                                          f"move {move_from} {random.choice(move_to_coords)}")):
                break


if __name__ == "__main__":
    main("1")
