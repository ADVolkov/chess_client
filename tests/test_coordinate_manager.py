from chess_client import CoordinateManager, is_game_over, is_success_connect, main
import pytest


def test_pawn():
    assert CoordinateManager("P", "c3").get_correct_moves() == ["c4", "d4", "b4"]
    assert CoordinateManager("p", "e6").get_correct_moves() == ["e5", "f5", "d5"]


def test_possession():
    assert CoordinateManager("r", "c3").get_correct_moves() == ['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'a3',
                                                                'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3']
    assert CoordinateManager("R", "c3").get_correct_moves() == ['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'a3',
                                                                'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3']


def test_knight():
    assert CoordinateManager("n", "c3").get_correct_moves() == ['b5', 'd5', 'e4', 'a4', 'b1', 'd1', 'e2', 'a2']
    assert CoordinateManager("N", "c3").get_correct_moves() == ['b5', 'd5', 'e4', 'a4', 'b1', 'd1', 'e2', 'a2']


def test_bishop():
    assert CoordinateManager("b", "c3").get_correct_moves() == ['d4', 'e5', 'f6', 'g7', 'h8', 'b4', 'a5', 'b2', 'a1',
                                                                'd2', 'e1']
    assert CoordinateManager("B", "c3").get_correct_moves() == ['d4', 'e5', 'f6', 'g7', 'h8', 'b4', 'a5', 'b2', 'a1',
                                                                'd2', 'e1']


def test_king():
    assert CoordinateManager("k", "c3").get_correct_moves() == ['b4', 'b2', 'd4', 'd2', 'c4', 'd3', 'c2', 'b3']
    assert CoordinateManager("K", "c3").get_correct_moves() == ['b4', 'b2', 'd4', 'd2', 'c4', 'd3', 'c2', 'b3']


def test_queen():
    assert CoordinateManager("q", "c3").get_correct_moves() == ['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'a3',
                                                                'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3', 'd4', 'e5',
                                                                'f6', 'g7', 'h8', 'b4', 'a5', 'b2', 'a1', 'd2', 'e1']
    assert CoordinateManager("Q", "c3").get_correct_moves() == ['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'a3',
                                                                'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3', 'd4', 'e5',
                                                                'f6', 'g7', 'h8', 'b4', 'a5', 'b2', 'a1', 'd2', 'e1']


def test_incorrect_figure():
    with pytest.raises(ValueError):
        assert CoordinateManager("c", "c3").get_correct_moves() == "Figure is incorrect"


def test_incorrect_coordinates():
    with pytest.raises(ValueError):
        assert CoordinateManager("f", "c0").get_correct_moves() == "Coordinates are incorrect"


def test_is_game_over():
    assert is_game_over("victory")
    assert not is_game_over("success")


def test_is_success_connect():
    assert is_success_connect("new_room_created")
    assert is_success_connect("successfully_connected_to_room")
    assert not is_success_connect("success")


def test_main():
    with pytest.raises(ConnectionRefusedError):
        assert main(1) == "[Errno 61] Connection refused"
